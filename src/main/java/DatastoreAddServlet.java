import java.io.IOException;

import java.util.logging.Logger;

import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;

import com.google.appengine.api.datastore.DatastoreServiceFactory;

import com.google.appengine.api.datastore.Entity;

import com.google.appengine.api.datastore.Key;

import com.google.appengine.api.datastore.KeyFactory;

import com.google.appengine.api.users.User;

import com.google.appengine.api.users.UserService;

import com.google.appengine.api.users.UserServiceFactory;

import java.util.Date;

 

public class DatastoreAddServlet extends HttpServlet

{

                  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException

                  {

        resp.setContentType("text/html");

        resp.getWriter().print("<HTML>");

        resp.getWriter().print("<BODY>");

        resp.getWriter().print("<FORM ACTION=\"/add\" METHOD=POST>");

        resp.getWriter().print("<p>Name: <input type=\"text\" name=\"name\"></p>");

        resp.getWriter().print("<p>Email: <input type=\"text\" name=\"email\"></p>");

        resp.getWriter().print("<p>Rank(assign a number 1-5): <input type=\"text\" name=\"rank\"></p>");

        resp.getWriter().print("</br>");

        resp.getWriter().print("<input type=\"submit\" value=\"send\">");

        resp.getWriter().print("</FORM>");

        resp.getWriter().print("</BODY>");

        resp.getWriter().print("</HTML>");      

                  }

                 

                  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException

                  {

                  UserService userService = UserServiceFactory.getUserService();

                  User user = userService.getCurrentUser();

 

        Key contactKey = KeyFactory.createKey("Contact", req.getParameter("name"));

        Date date = new Date();

        Entity contact = new Entity("Contact", contactKey);

        contact.setProperty("contactName", req.getParameter("name"));

        contact.setProperty("created", date);

        contact.setProperty("email", req.getParameter("email"));

        contact.setProperty("rank", req.getParameter("rank"));

            

 

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        datastore.put(contact);       

       

        resp.getWriter().println("Contact Created.");

                  }

}