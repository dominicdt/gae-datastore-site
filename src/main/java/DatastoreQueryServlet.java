import java.io.IOException;

import java.util.logging.Logger;

import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;

import com.google.appengine.api.datastore.DatastoreServiceFactory;

import com.google.appengine.api.datastore.Entity;

import com.google.appengine.api.datastore.FetchOptions;

import com.google.appengine.api.datastore.Key;

import com.google.appengine.api.datastore.KeyFactory;

import com.google.appengine.api.datastore.Query;

import com.google.appengine.api.users.User;

import com.google.appengine.api.users.UserService;

import com.google.appengine.api.users.UserServiceFactory;

import java.util.Date;

import java.util.List;

 

public class DatastoreQueryServlet extends HttpServlet

{

                  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException

                  {

                                    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

                                    Query query = new Query("Contact");

                      List<Entity> contacts = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());

        resp.setContentType("text/plain");

 

                      for (Entity contact : contacts)

                      {

                          resp.getWriter().println( "Contact: " + contact.getProperty("contactName") + ", Contact Email: " + contact.getProperty("email") + ", Rank: " + contact.getProperty("rank").toString());

                      }                             

                  }

}